import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {User} from 'app/typings/store/user';
import {Http} from '@angular/http';

@Injectable()
export class UserService {

  constructor(private _http: Http) { }

  public getUserInfo(id: string): Observable<User> {
    return this._http.get(`http://localhost:3000/users/${id}`).map((users) => {
      return users.json();
    });
  }
}
