import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public isMenuOpen: boolean;

  constructor(private store: Store<any>) { }

  public ngOnInit(): void {
    this.store.subscribe((state) => {
      this.isMenuOpen = state.menu.isMenuOpen;
    });
  }
}
