import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Store, StoreModule} from '@ngrx/store';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HeaderModule} from './modules/header/header.module';
import {MaterialModule} from 'app/shared/material/material.module';
import {PostModule} from 'app/modules/post/post.module';
import {RouterModule} from '@angular/router';
import {routes} from 'app/app.routing';
import { HomeComponent } from './modules/home/home.component';

import {reducers} from './app.store';
import {MenuComponent} from 'app/modules/menu/menu.component';
import {UserCenterModule} from 'app/modules/user-center/user-center.module';
import { CounterComponent } from './modules/counter/counter.component';
import {EffectsModule} from '@ngrx/effects';
import {CounterEffect} from 'app/store/counter/counter.effect';
import {MdSnackBar, MdSnackBarModule} from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    CounterComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    HeaderModule,
    PostModule,
    RouterModule.forRoot(routes),
    UserCenterModule,
    StoreModule.provideStore(reducers),
    EffectsModule.run(CounterEffect)
  ],
  providers: [MdSnackBarModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
