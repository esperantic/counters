import {IAction} from 'app/store/interfaces/action.interface';
import {User} from 'app/typings/store/user';

interface ChangeUserAction extends IAction {
  payload: User;
}

export class UserActions {
  public static CHANGE_USER = 'CHANGE_USER';

  public static changeUser(user: User): ChangeUserAction {
    return {
      type: UserActions.CHANGE_USER,
      payload: user
    }
  }
}
