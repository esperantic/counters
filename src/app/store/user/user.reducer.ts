import {User} from 'app/typings/store/user';

import {TReducer} from 'app/store/interfaces/reducer.interface';
import {UserActions} from 'app/store/user/user.actions';

function updateUserState(state, user: User) {
  return {...state, ...user};
}

const defaultUser: User = {
  name: 'Unauthorized',
  permissions: []
};

export const userReducer: TReducer<User> = function(state = defaultUser, action) {
  switch (action.type) {
    case UserActions.CHANGE_USER:
      return updateUserState(state, action.payload);
    default:
      return state;
  }
};
