import {IAction} from 'app/store/interfaces/action.interface';

export type TReducer<T> = (state: T, action: IAction) => T;
