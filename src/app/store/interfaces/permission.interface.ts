export interface IPermission {
  name: string;
  value: boolean;
}
