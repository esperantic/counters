import {Action} from '@ngrx/store';
import {IPermission} from 'app/store/interfaces/permission.interface';

export class StoreAction implements Action {
  public type: string;
  public payload?: any;
  public permissions: Array<IPermission>;

  constructor(type: string, payload: any = {}, permissions: IPermission[] = []) {
    this.type = type;
    this.payload = payload;
    this.permissions = permissions;
  }
}
