import {IAction} from 'app/store/interfaces/action.interface';
import {Action} from '@ngrx/store';



export class MenuActions {
  static TOGGLE_MENU = 'TOGGLE_MENU';
  static toggleMenu(): IAction {
   return {
     type: MenuActions.TOGGLE_MENU
   }
  }
}
