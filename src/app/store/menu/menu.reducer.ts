// import { ActionReducer, Action } from '@ngrx/store';
import {MenuActions} from './menu.actions';
import {Menu} from 'app/store/menu/menu.interface';
import {TReducer} from 'app/store/interfaces/reducer.interface';

export const menuReducer: TReducer<Menu> = function(state = {isMenuOpen: false}, action) {
  switch (action.type) {
    case MenuActions.TOGGLE_MENU:
      return {...state, isMenuOpen: !state.isMenuOpen};
    default:
      return state;
  }
};
