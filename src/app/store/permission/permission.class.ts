import {User} from 'app/typings/store/user';
export class Permissions {
  static checkPermissions(user: User, requiredPermissions: Array<string>): boolean {
    console.log(user, requiredPermissions);
    return user.permissions.some(
      (availablePermission) => (
        requiredPermissions.some((requiredPermission) => availablePermission === requiredPermission)
      )
    );
  }
}
