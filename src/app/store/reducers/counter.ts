import { ActionReducer, Action } from '@ngrx/store';
import {Counter} from 'app/typings/store/counter';
import {CounterActions} from 'app/store/counter/counter.action';

const defaultCounter: Counter = {
  value: 0,
  error: ''
};

export function counterReducer(state: Counter = defaultCounter, action: Action) {
  switch (action.type) {
    case CounterActions.INCREMENT_COUNTER:
      return {...state, value: state.value + 1, error: ''};
    case CounterActions.DECREMENT_COUNTER:
      return {...state, value: state.value - 1, error: ''};
    case CounterActions.PERMISSION_DENIED:
      console.log(action);
      return {...state, error: action.payload.error};
    default:
      return state;
  }
}
