import {IAction} from 'app/store/interfaces/action.interface';
export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';

interface IUserCounterAction extends IAction {
  permissions: Array<string>,
  origin: IAction
}

interface ICounterAction extends IAction {
  permissions: Array<string>
}

export class CounterActions {
  static INCREMENT_COUNTER = 'INCREMENT_COUNTER';
  static DECREMENT_COUNTER = 'DECREMENT_COUNTER';
  static COUNTER_ACCESS = 'COUNTER_ACCESS';
  static PERMISSION_DENIED = 'PERMISSION_DENIED';

  private static nativeIncrement(): IAction {
    return {
      type: CounterActions.INCREMENT_COUNTER
    }
  }

  private static nativeDecrement(): IAction {
    return {
      type: CounterActions.DECREMENT_COUNTER
    }
  }

  static incrementCounter(): IUserCounterAction {
    return {
      type: CounterActions.COUNTER_ACCESS,
      permissions: ['INCREMENT', 'MANAGER_PERMISSION', 'ADMIN'],
      origin: CounterActions.nativeIncrement()
    };
  }

  static decrementCounter(): IUserCounterAction {
    return {
      type: CounterActions.COUNTER_ACCESS,
      permissions: ['DECREMENT', 'MANAGER_PERMISSION'],
      origin: CounterActions.nativeDecrement()
    }
  }

  static permissionDenied(payload): IAction {
    return {
      type: CounterActions.PERMISSION_DENIED,
      payload: payload
    }
  }

}


