import {Injectable} from '@angular/core';
import {Actions, Effect, toPayload} from '@ngrx/effects';
import {CounterActions} from 'app/store/counter/counter.action';
import {User} from 'app/typings/store/user';
import {Observable} from 'rxjs/Observable';
import {Action, Store} from '@ngrx/store';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/withLatestFrom';

import {Permissions} from 'app/store/permission/permission.class';
import {Counter} from 'app/typings/store/counter';

@Injectable()
export class CounterEffect {

  constructor(private actions$: Actions, private store$: Store<any>) { }

  @Effect() counterAccess$ = this.actions$
    .ofType(CounterActions.COUNTER_ACCESS)
    .withLatestFrom(this.store$)
    .switchMap(([action, store]) => {
      console.log(action, store);
      if (! Permissions.checkPermissions(store.user, action.permissions)) {
        return Observable.of(CounterActions.permissionDenied('Permission Denied'));
      }
      return Observable.of(action.origin);
    });


  // @Effect() incrementAction$ = this.actions$
  //   .ofType(CounterActions.INCREMENT_COUNTER)
  //   .withLatestFrom(this.store$)
  //   .switchMap(([action, store]) => {
  //     if (! Permissions.checkPermissions(store.user, action.permissions)) {
  //       console.warn('Permission Denied');
  //       return Observable.of(CounterActions.permissionDenied('Permission Denied'));
  //     }
  //     return Observable.of({type: ''});
  //   });

  // @Effect() decrementAccess$ = this.actions$
  //   .ofType(CounterActions.DECREMENT_COUNTER)
  //   .map(toPayload)
  //   .switchMap((user: User) => {
  //     return Observable.of();
  //   });
}
