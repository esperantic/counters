import {menuReducer} from './store/menu/menu.reducer';
import {userReducer} from 'app/store/user/user.reducer';
import {counterReducer} from 'app/store/reducers/counter';

export const reducers = {
  menu: menuReducer,
  user: userReducer,
  counter: counterReducer
};
