import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdButtonModule, MdCardModule, MdGridListModule, MdIconModule, MdListModule, MdMenuModule, MdSidenavModule, MdSnackBar,
  MdSnackBarModule,
  MdToolbarModule
} from '@angular/material';

const MATERIAL_MODULES = [
  CommonModule,
  BrowserAnimationsModule,
  MdButtonModule,
  MdMenuModule,
  MdCardModule,
  MdToolbarModule,
  MdIconModule,
  MdSidenavModule,
  MdGridListModule,
  MdListModule,
  MdSnackBarModule
];

@NgModule({
  imports: MATERIAL_MODULES,
  providers: [MdSnackBarModule],
  exports: MATERIAL_MODULES
})
export class MaterialModule { }
