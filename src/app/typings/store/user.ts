// import {Permission} from 'app/typings/store/permissions';

export interface User {
  name: string,
  permissions: Array<string>
}
