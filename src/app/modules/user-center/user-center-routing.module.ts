import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserCenterComponent} from './user-center.component';
import {UserComponent} from './user/user.component';
import {UserListComponent} from './user-list/user-list.component';

const routes: Routes = [
  {
    path: 'users',
    component: UserCenterComponent,
    children: [
      {
        path: '',
        component: UserListComponent,
      },
      {
        path: ':id',
        component: UserComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserCenterRoutingModule { }
