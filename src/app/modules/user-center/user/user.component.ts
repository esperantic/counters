import { Component, OnInit } from '@angular/core';
import {Action, Store} from '@ngrx/store';
import {CounterActions} from 'app/store/counter/counter.action';
import {ActivatedRoute} from '@angular/router';
import {User} from 'app/typings/store/user';
import {UserService} from 'app/services/user.service';
import {MdSnackBar} from '@angular/material';
import {UserActions} from 'app/store/user/user.actions';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public user: User;

  public increment = CounterActions.incrementCounter();
  public decrement = CounterActions.decrementCounter();

  constructor(
    private store: Store<any>,
    private route: ActivatedRoute,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(parameters => {
      this.userService.getUserInfo(parameters.id).subscribe((user: User): void => {
        this.store.dispatch(UserActions.changeUser(user));
      });
    });
    this.store.select('user').subscribe((user: User): void => {
      this.user = user;
    });
  }

  counterChange(action: Action): void {
    this.store.dispatch(action);
  }
}
