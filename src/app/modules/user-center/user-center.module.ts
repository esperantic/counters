import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserCenterRoutingModule } from './user-center-routing.module';
import { UserCenterComponent } from './user-center.component';
import { UserComponent } from './user/user.component';
import { UserListComponent } from './user-list/user-list.component';
import {MaterialModule} from 'app/shared/material/material.module';
import {UserService} from 'app/services/user.service';

@NgModule({
  imports: [
    CommonModule,
    UserCenterRoutingModule,
    MaterialModule
  ],
  declarations: [UserCenterComponent, UserComponent, UserListComponent],
  providers: [UserService],
  exports: [UserCenterComponent, UserComponent]
})
export class UserCenterModule { }
