import { Component, OnInit } from '@angular/core';
import {MenuActions} from '../../store/menu/menu.actions';
import {Store} from '@ngrx/store';
import {Menu} from 'app/store/menu/menu.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public isMenuOpen: boolean;

  public user: string;

  constructor(private store: Store<any>) { }

  ngOnInit(): void {
    this.store.select('menu').subscribe((state: Menu) => {
      this.isMenuOpen = state.isMenuOpen;
      console.log('Header component menu state updated');
    });
  }

  public toggleMenu() {
    this.store.dispatch(MenuActions.toggleMenu());
  }
}
