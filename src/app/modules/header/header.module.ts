import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import {MaterialModule} from 'app/shared/material/material.module';
import {RouterModule} from '@angular/router';
import {Store, StoreModule} from '@ngrx/store';
import {menuReducer} from '../../store/menu/menu.reducer';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    StoreModule.provideStore({menu: menuReducer})
  ],
  declarations: [HeaderComponent],
  exports: [HeaderComponent],
})
export class HeaderModule { }
