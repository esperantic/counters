import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {Counter} from 'app/typings/store/counter';
import {MdSnackBar} from '@angular/material';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  public counter: number;

  constructor(
    private store: Store<any>,
    private mdSnack: MdSnackBar
  ) { }

  ngOnInit() {
    this.store.select('counter').subscribe((state: Counter) => {
      this.counter = state.value;
      if (state.error !== '') {
        this.mdSnack.open(state.error, 'Ok', {duration: 5000});
      }
    })
  }

}
