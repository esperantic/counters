import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
    title = 'Post List';
    public myData: Array<any>;

    constructor(private http: Http, private router: Router) {
        this.http.get('https://jsonplaceholder.typicode.com/photos')
          .map(response => response.json())
          .subscribe(res => this.myData = res);
    }

    ngOnInit() {
    }
}
