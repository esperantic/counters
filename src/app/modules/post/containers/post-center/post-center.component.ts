import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-post-center',
  templateUrl: './post-center.component.html',
  styleUrls: ['./post-center.component.css']
})
export class PostCenterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
