import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostComponent } from './containers/post/post.component';
import { PostListComponent } from './containers/post-list/post-list.component';
import { PostCenterComponent } from './containers/post-center/post-center.component';
import {PostRoutingModule} from 'app/modules/post/post.routing.module';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostRoutingModule
  ],
  declarations: [PostComponent, PostListComponent, PostCenterComponent],
  exports: [PostComponent, PostListComponent, PostCenterComponent, RouterModule]
})
export class PostModule { }
