import {Route, RouterModule, Routes} from '@angular/router';
import {PostComponent} from 'app/modules/post/containers/post/post.component';
import {PostListComponent} from 'app/modules/post/containers/post-list/post-list.component';
import {PostCenterComponent} from 'app/modules/post/containers/post-center/post-center.component';
import {NgModule} from '@angular/core';

const postRoutes: Routes = [
  {
    path: 'posts',
    component: PostCenterComponent,
    children: [
      {
        path: '',
        component : PostListComponent
      },
      {
        path: ':id',
        component : PostComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(postRoutes)],
  exports: [RouterModule]
})
export class PostRoutingModule {}
