import { PrepairPage } from './app.po';

describe('prepair App', () => {
  let page: PrepairPage;

  beforeEach(() => {
    page = new PrepairPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
